This Alpine Linux base image comes with [bats](https://github.com/sstephenson/bats) and [gosu](https://github.com/tianon/gosu).
